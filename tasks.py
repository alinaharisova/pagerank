import requests
from bs4 import BeautifulSoup
import operator
import threading

class Tasks:

    def __init__(self, start_url, size):
        self.size = size # размер матрицы
        self.uq_links = [start_url] # ссылка сайта
        self.data = {} 
        self.page_rank = {}

    def fill_unique_links(self):
        html = ''
        while (len(self.uq_links) < self.size):
                url = self.uq_links[-1]
                if (url[-1] == '/'):
                    url = url[:-1]
                try:
                    html = requests.get(url).text
                    success = True
                    soup = BeautifulSoup(html, "html.parser")
                    for item in soup.find_all('a'):
                        href = item.get('href')
                        if href is not None and href not in self.uq_links and len(href) > 2:
                            if not href.find('http'):
                                self.uq_links.append(href)
                            else:
                                if (href[0] == '/'):
                                    href = url + href
                                else:
                                    href = url + '/' + href
                        if (len(self.uq_links) >= self.size):
                            break
                except requests.exceptions.RequestException as e:
                    print('%s not correct\n' % self.uq_links[-1])
                    self.uq_links = self.uq_links[:-1]

    def fill_page_links(self):
        for link in self.uq_links:
            html = requests.get(link).text
            soup = BeautifulSoup(html, "html.parser")
            items = soup.find_all('a')
            hrefs = []
            for item in items:
                hrefs.append(item.get('href'))
            self.data[link] = hrefs

    # построение матрицы смежности
    def make_matrix(self):
        self.fill_unique_links()
        self.fill_page_links()
        matrix = []
        i = 0
        for inner_link in self.uq_links:
            i = i + 1
            item = []
            index = 0
            for page_link in self.uq_links:
                if inner_link in self.data.get(page_link, []):
                    item.append(index)
                    index = index + 1
            matrix.append(item)
        self.matrix = matrix

    # вычисление PageRank
    def calculate_page_rank(self):
        for link in self.uq_links:
            self.page_rank[link] = 0
        i = 0
        for page_link in self.matrix:
            for has_link in page_link:
                if has_link:
                    self.page_rank[self.uq_links[i]] = self.page_rank[self.uq_links[i]] + 1
            i = i + 1
        self.page_rank = sorted(self.page_rank.items(), key = operator.itemgetter(1), reverse = True)


    # вычисление PageRank с использованием параллельности
    def page_rank_parallel(self):
        for link in self.uq_links:
            self.page_rank[link] = 0
            test = self.parts(2)
            # инициализируем два потока
            t1 = threading.Thread(target = self.calc, args = (0, len(test[0]) - 1, test[0]))
            t2 = threading.Thread(target = self.calc, args = (len(test[0]) - 1, len(test[0]) - 1 + len(test[1]), test[1]))
            # запускаем потоки
            t1.start()
            t2.start()
            t1.join()
            t2.join()
        self.page_rank = sorted(self.page_rank.items(), key = operator.itemgetter(1), reverse = True)

    def calc(self, s_index, f_index, matrix):
        for i in range(s_index, f_index):
            self.page_rank[self.uq_links[i]] = len(self.matrix[i])   

    def parts(self, part_count):
        lst = self.matrix
        part_size = len(lst) // part_count
        return [lst[i : i + part_size] for i in range(0, len(lst), part_size)]

    # вывод в файл
    def print_result(self, file):
        f = open(file, 'w')
        i = 0
        first_line = ''
        for l in self.uq_links:
            i = i + 1
            str = '%03d ' % i
            first_line = first_line + str
            f.write('%03d=<%s> \n' % (i, l))
        f.write('\n    %s\n' % first_line)
        for page_link in self.matrix:
            i = i + 1
            out_string = '%03d' % i
        for inner_index in range(len(page_link)):
            if (inner_index in page_link):
                out_string = out_string + '   1'
            else:
                out_string = out_string + '   0'
            f.write(out_string + '\n')
        for l in self.page_rank:
            f.write('%s=<%s> \n' % (l[0],l[1]))
        f.close()